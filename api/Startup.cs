﻿using api.BackgroundTasks.TwitterTask;
using api.Migrations;
using api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sieve.Services;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration["ConnectionStrings:MySql"];
            services.AddDbContext<ApplicationContext>(
                b => b
//                    .UseLazyLoadingProxies()
                    .UseMySql(connectionString));

            services.AddTransient<UserRepository, UserRepository>();
            services.AddTransient<TwitterClientService, TwitterClientService>();
            services.AddTransient<TweetRepository, TweetRepository>();
            services.AddSingleton<IHostedService, TwitterTask>();

            services.AddScoped<SieveProcessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(builder =>
                builder
                    .AllowAnyOrigin()
                    .AllowCredentials()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );

            app.UseMvc();
        }
    }
}