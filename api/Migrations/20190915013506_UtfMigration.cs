﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class UtfMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Text",
                table: "Tweets",
                newName: "text");

            migrationBuilder.AlterColumn<string>(
                name: "text",
                table: "Tweets",
                type: "text character set utf8mb4 COLLATE utf8mb4_polish_ci",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "text",
                table: "Tweets",
                newName: "Text");

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                table: "Tweets",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text character set utf8mb4 COLLATE utf8mb4_polish_ci",
                oldNullable: true);
        }
    }
}