using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

namespace api.Services
{
    public class TwitterClientService
    {
        private readonly RestClient _restClient;
        private string token;

        private string twitterKey;
        private string twitterSecretKey;

        public TwitterClientService()
        {
            twitterKey = Startup.Configuration["Twitter:Key"];
            twitterSecretKey = Startup.Configuration["Twitter:KeySecret"];

            Console.WriteLine("new twitter client");
            _restClient = new RestClient("https://api.twitter.com");
        }

        public async Task<List<TweetDao>> GetTweetsWithHashtag(string hashtag)
        {
            if (token == null)
            {
                token = await Login();
            }

            var request = new RestRequest("1.1/search/tweets.json");

            request.AddHeader("Accept", "application/json");

            request.AddParameter("q", hashtag);
            request.AddHeader("Authorization", $"Bearer {token}");
            request.Timeout = 10000; // 10 s

            var response = await _restClient.ExecuteTaskAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new SystemException("get tweets exception " + response.StatusCode + response.Content);
            }


            var result = JObject.Parse(response.Content)["statuses"]
                .Children()
                .Select((jToken, i) =>
                {
                    var text = ((string) jToken["text"]).Split("http")[0];

                    return new TweetDao()
                    {
                        Id = (string) jToken["id_str"],
                        UserId = (string) (jToken["user"]["id_str"]),
                        UserName = (string) (jToken["user"]["name"]),
                        ImageUrl = jToken["entities"]["media"] == null
                            ? null
                            : (string) (jToken["entities"]["media"][0]["media_url_https"]),
                        Date = DateTime.ParseExact((string) jToken["created_at"], "ddd MMM dd HH:mm:ss +ffff yyyy",
                            CultureInfo.InvariantCulture),
                        Text = text
                    };
                })
                .ToList();

            return result;
        }

        private async Task<string> Login()
        {
            var loginClient = new RestClient("https://api.twitter.com");
            loginClient.Authenticator = new HttpBasicAuthenticator(twitterKey, twitterSecretKey);
            var request = new RestRequest("oauth2/token");
            request.AddParameter("grant_type", "client_credentials");
            request.Method = Method.POST;

            var response = await loginClient.ExecuteTaskAsync(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new SystemException("login error, " + response.StatusCode + response.Content);
            }

            var jwt = (string) JObject.Parse(response.Content)["access_token"];

            Console.WriteLine("logged in");
            return jwt;
        }
    }

    public class TweetDao
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ImageUrl { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}