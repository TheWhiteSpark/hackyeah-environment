using System.Linq;
using Sieve.Models;
using Sieve.Services;

namespace api.Utils
{
    public static class SieveExtensions
    {
        public static IQueryable<T> ApplySieve<T>(this IQueryable<T> queryable, SieveProcessor sieveProcessor,
            SieveModel sieveModel)
        {
            return sieveProcessor.Apply(sieveModel, queryable);
        }
    }
}