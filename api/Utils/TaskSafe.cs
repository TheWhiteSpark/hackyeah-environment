using System;
using System.Threading;
using System.Threading.Tasks;

namespace api.Utils
{
    public static class TaskSafe
    {
        public static async Task Delay(TimeSpan span, CancellationToken token)
        {
            try
            {
                await Task.Delay(span, token);
            }
            catch (OperationCanceledException)
            {
            }
        }

        public static async Task Delay(int time, CancellationToken token)
        {
            try
            {
                await Task.Delay(time, token);
            }
            catch (OperationCanceledException)
            {
            }
        }
    }
}