using System;
using System.Threading;
using System.Threading.Tasks;
using api;
using api.Migrations;
using api.Services;
using api.Utils;
using Microsoft.Extensions.DependencyInjection;

namespace api.BackgroundTasks.TwitterTask
{
    public class TwitterTask : HostedTask
    {
        private readonly IServiceProvider _serviceProvider;
        private TwitterClientService _twitterClientService;
        private TweetRepository _tweetRepository;

        public TwitterTask(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public override async Task Execute(CancellationToken token)
        {
            var scope = ServiceProvider.CreateScope();
            _twitterClientService = scope.ServiceProvider.GetRequiredService<TwitterClientService>();
            _tweetRepository = scope.ServiceProvider.GetRequiredService<TweetRepository>();

            while (!token.IsCancellationRequested)
            {
                Console.Out.WriteLine("Syncing tweets");

                var list = await _twitterClientService.GetTweetsWithHashtag(Startup.Configuration["Hashtag"]);

                foreach (var item in list)
                {
                    _tweetRepository.AddTweet(item);
                }

                await TaskSafe.Delay(TimeSpan.FromSeconds(5), token);
            }

            await TaskSafe.Delay(TimeSpan.FromSeconds(1), token);
        }
    }
}