using System;
using System.Threading;
using System.Threading.Tasks;
using api.Utils;
using Microsoft.Extensions.Hosting;

namespace api.BackgroundTasks
{
    public abstract class HostedTask : IHostedService
    {
        protected readonly IServiceProvider ServiceProvider;
        private CancellationTokenSource cts;
        private Task executingTask;

        public HostedTask(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            executingTask = ExecuteTask(cts.Token);
            return executingTask.IsCompleted ? executingTask : Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            if (executingTask == null)
            {
                return;
            }

            cts.Cancel();
            await Task.WhenAny(executingTask, TaskSafe.Delay(Timeout.Infinite, cancellationToken));
            // cancellationToken.ThrowIfCancellationRequested();
        }

        public abstract Task Execute(CancellationToken token);

        private async Task ExecuteTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    await Execute(token);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    await TaskSafe.Delay(1000 * 5, token);
                }
            }
        }
    }
}