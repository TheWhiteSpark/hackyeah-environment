using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Entities
{
    public class Tweet
    {
        [Key] public string Id { get; set; }
        public User User { get; set; }
        public string ImageUrl { get; set; }
        public DateTime Date { get; set; }

        [Column("text", TypeName = "text character set utf8mb4 COLLATE utf8mb4_polish_ci")]
        public string Text { get; set; }

        public int NumberOfLikes { get; set; }
    }
}