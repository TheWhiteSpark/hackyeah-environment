using System.ComponentModel.DataAnnotations;

namespace api.Entities
{
    public class User
    {
        [Key] public string Id { get; set; }
        public string Username { get; set; }
    }
}