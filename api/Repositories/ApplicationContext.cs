using api.Entities;
using Microsoft.EntityFrameworkCore;

namespace api.Migrations
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
            Database.Migrate();
        }

        public ApplicationContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Tweet> Tweets { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}