using System;
using System.Collections.Generic;
using System.Linq;
using api.Entities;
using api.Services;
using api.Utils;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using Sieve.Services;

namespace api.Migrations
{
    public class TweetRepository
    {
        private readonly ApplicationContext _context;
        private readonly SieveProcessor _sieveProcessor;

        public TweetRepository(ApplicationContext context, SieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
        }

        public ActionResult<IEnumerable<Tweet>> GetTopTweets(int numberOfTweets)
        {
            return _context.Tweets
                .OrderByDescending(tweet => tweet.NumberOfLikes)
                .Take(numberOfTweets)
                .ToList();
        }

        public Tweet GetRandom()
        {
            return _context.Tweets.ToList().ElementAt(new Random().Next(_context.Tweets.Count()));
        }

        public ActionResult<IEnumerable<Tweet>> GetTweets(SieveModel sieveModel)
        {
            return _context.Tweets
                .OrderByDescending(tweet => tweet.Date)
                .ApplySieve(_sieveProcessor, sieveModel)
                .ToList();
        }

        public void AddTweet(TweetDao item)
        {
            var tweet = _context.Tweets.Find(item.Id);
            if (tweet == null)
            {
                var user = _context.Users.Find(item.UserId);

                if (user == null)
                {
                    user = new User()
                    {
                        Id = item.UserId,
                        Username = item.UserName
                    };
                    _context.Users.Add(user);
                    Console.WriteLine("new user added: " + item.UserName);
                }

                _context.Tweets.Add(new Tweet()
                {
                    Id = item.Id,
                    User = user,
                    NumberOfLikes = 0,
                    Text = item.Text,
                    Date = item.Date,
                    ImageUrl = item.ImageUrl
                });
                _context.SaveChanges();
                Console.WriteLine("new tweet added: " + item.Id);
            }
        }

        public void IncrementLike(string id)
        {
            var tweet = _context.Tweets.Find(id);
            if (tweet != null)
            {
                tweet.NumberOfLikes++;

                _context.Tweets.Update(tweet);

                _context.SaveChanges();
                Console.WriteLine("liked tweet: " + id);
            }
            else
            {
                throw new ArgumentException("no tweet with id: " + id);
            }
        }

        public int GetTweetsCount()
        {
            return _context.Tweets.Count();
        }

        public int GetTotalLikesCount()
        {
            return _context.Tweets.Sum(tweet => tweet.NumberOfLikes);
        }
    }
}