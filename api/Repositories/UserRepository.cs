using System.Linq;

namespace api.Migrations
{
    public class UserRepository
    {
        private readonly ApplicationContext _context;

        public UserRepository(ApplicationContext context)
        {
            _context = context;
        }

        public int GetUserCount()
        {
            return _context.Users.Count();
        }
    }
}