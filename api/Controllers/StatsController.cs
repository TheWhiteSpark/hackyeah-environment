using api.Migrations;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class StatsController : ControllerBase
    {
        private readonly TweetRepository _tweetRepository;
        private readonly UserRepository _userRepository;

        public StatsController(TweetRepository tweetRepository, UserRepository userRepository)
        {
            _tweetRepository = tweetRepository;
            _userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult<Stats> GetStats()
        {
            return new Stats()
            {
                Posts = _tweetRepository.GetTweetsCount(),
                Users = _userRepository.GetUserCount(),
                Likes = _tweetRepository.GetTotalLikesCount()
            };
        }
    }

    public class Stats
    {
        public int Posts { get; set; }
        public int Likes { get; set; }
        public int Users { get; set; }
    }
}