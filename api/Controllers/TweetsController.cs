using System.Collections.Generic;
using api.Entities;
using api.Migrations;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class TweetsController : ControllerBase
    {
        private readonly TweetRepository _tweetRepository;

        public TweetsController(TweetRepository tweetRepository)
        {
            _tweetRepository = tweetRepository;
        }

        [HttpGet]
        [Route("top")]
        public ActionResult<IEnumerable<Tweet>> GetTop([FromQuery] SieveModel sieveModel)
        {
            return _tweetRepository.GetTopTweets(10);
        }

        [HttpGet]
        [Route("random")]
        public ActionResult<Tweet> GetRandom()
        {
            return _tweetRepository.GetRandom();
        }

        [HttpGet]
        public ActionResult<IEnumerable<Tweet>> Get([FromQuery] SieveModel sieveModel)
        {
            return _tweetRepository.GetTweets(sieveModel);
        }

        [HttpPost]
        [Route("{id}")]
        public void LikeTweet([FromRoute] string id)
        {
            _tweetRepository.IncrementLike(id);
        }
    }
}