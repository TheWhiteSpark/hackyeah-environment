const Rest = {
    getStatistics,
    getPage,
    getBest,
    getTop,
    like,
};
const baseUrl = "http://vps727632.ovh.net:5000/api/";

function get(url) {

  const requestOptions = {
    method: 'get',
    headers: { 'Content-Type': 'application/json'},
    responseType: 'application/json',
  };
  return fetch(baseUrl + url, requestOptions)
  .then(function(response) {
    return response.json();
  });
}

function getStatistics() {
  return get('stats');
  // return Promise.resolve({
  //   posts: 12343,
  //   likes: 123566,
  //   users: 876,
  // });
}

function getPage(number, size) {
  return get(`tweets?page=${number}&pageSize=${size}`);
}

function getBest() {
  return get('tweets/random');
}

function getTop() {
  return get('tweets/top');
}

function like(id) {
  const requestOptions = {
    method: 'post',
    headers: { 'Content-Type': 'application/json'},
    responseType: 'application/json',
  };
  return fetch(baseUrl + 'tweets/'+ id, requestOptions);
}
export default  Rest;