import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { PostComponent } from './post/post.component';
import { TopComponent } from './top/top.component';
import { VoteComponent } from './vote/vote.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MainComponent } from './main/main.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NouisliderModule,
        RouterModule,
        JwBootstrapSwitchNg2Module,
        InfiniteScrollModule,
    ],
    declarations: [
        ComponentsComponent,
        MainComponent,
        PostComponent,
        TopComponent,
        VoteComponent,
    ],
    entryComponents: [],
    exports:[ ComponentsComponent ],
    providers: [
    ]
})
export class ComponentsModule { }
