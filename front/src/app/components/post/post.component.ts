import { Component, OnInit, Input } from '@angular/core';
import  Rest  from '../../rest';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() post: Post;
    constructor() { }

    ngOnInit() {
    }

    like() {
      let likesStr = localStorage.getItem('likes');
      let likes = []
      if(likesStr) {
        likes = JSON.parse(likesStr);
      }
      if (likes.findIndex(e => e == this.post.id) == -1) {
        Rest.like(this.post.id);
        this.post = {...this.post, numberOfLikes: this.post.numberOfLikes +1};
        likes = [...likes, this.post.id];11
        localStorage.setItem('likes', JSON.stringify(likes));
      }
    }
}
export interface Post {
  id: string;
  numberOfLikes: number;
  imageUrl: string;
  text: string;
}