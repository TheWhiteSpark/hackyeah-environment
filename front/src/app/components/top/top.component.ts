import { Component, OnInit } from '@angular/core';
import Rest from '../../rest';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {
  posts = [];  
  constructor() { }

    ngOnInit() {
      Rest.getTop().then(r => {
        this.posts = r;
      });
    }
}
