import { Component, OnInit } from '@angular/core';
import Rest from '../../rest';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    
    stats = { posts:0, users:0, likes: 0}
    bestPost = undefined;
    constructor() { }


    ngOnInit() {
      Rest.getBest().then(r => {
        this.bestPost = r;
      });
      Rest.getStatistics().then(s => {
        this.stats = s;
      });
    }
}
