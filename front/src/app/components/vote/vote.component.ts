import { Component, OnInit, HostListener } from '@angular/core';
import Rest from '../../rest';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit {
  page = 1;
  pageSize = 10;
  posts = [];
  ended = false;
  constructor() { }

    gotoPage(newPage: number) {
      this.page = newPage;
      this.getPage(newPage, this.pageSize).then(r => {
        if (r.length == 0) {
          this.ended = true;
        } else {
          this.posts = [...this.posts, ...r];
        }
      });
    }
    ngOnInit() {
      this.getPage(this.page, this.pageSize).then(r => {
        this.posts = [...this.posts, ...r];
      });
    }

    onScroll() {
      if(!this.ended)
        this.gotoPage(this.page+1);
    }

    getPage(page: number, size: number) {
      return Rest.getPage(page, this.pageSize);
      // if(page >10) {
      //   return Promise.resolve([]);
      // } else 
      // return Promise.resolve([
      //     {
      //       id: 'id',
      //       likes: 123,
      //       imgUrl: 'https://loremflickr.com//400/300',
      //       description: 'qegt ewkth isd kugdhrssr',
      //     },
      //     {
      //       id: 'id',
      //       likes: 123,
      //       imgUrl: 'https://source.unsplash.com/random',
      //       description: 'qegt ewkth isd kugdhrssr',
      //     },
      //     {
      //       id: 'id',
      //       likes: 123,
      //       imgUrl: 'https://picsum.photos/400/300',
      //       description: 'qegt ewkth isd kugdhrssr',
      //     },
      //     {
      //       id: 'id',
      //       likes: 123,
      //       imgUrl: 'https://source.unsplash.com/random',
      //       description: 'qegt ewkth isd kugdhrssr',
      //     },
      //     {
      //       id: 'id',
      //       likes: 123,
      //       imgUrl: 'https://picsum.photos/400/300',
      //       description: 'qegt ewkth isd kugdhrssr',
      //     }
      //   ]);
    }
}
